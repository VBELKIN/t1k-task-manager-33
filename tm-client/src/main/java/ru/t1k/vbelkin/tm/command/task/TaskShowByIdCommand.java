package ru.t1k.vbelkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.dto.request.TaskGetByIdRequest;
import ru.t1k.vbelkin.tm.model.Task;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final Task task = getTaskEndpointClient().getTaskById(request).getTask();
        showTask(task);
    }

}
