package ru.t1k.vbelkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.api.model.ICommand;
import ru.t1k.vbelkin.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal command.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@Nullable final ICommand command : commands) System.out.println(command);
    }

}
