package ru.t1k.vbelkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator comparator);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    M add(M model);

    boolean existsById(String id);

    @Nullable
    M findOneById(@Nullable String id);

    void removeAll(@Nullable Collection<M> collection);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    Integer getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

}
