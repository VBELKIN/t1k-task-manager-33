package ru.t1k.vbelkin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlSaveJaxbRequest extends AbstractUserRequest {

    public DataXmlSaveJaxbRequest(@Nullable String token) {
        super(token);
    }

}
