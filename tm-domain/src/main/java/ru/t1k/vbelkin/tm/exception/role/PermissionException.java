package ru.t1k.vbelkin.tm.exception.role;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
