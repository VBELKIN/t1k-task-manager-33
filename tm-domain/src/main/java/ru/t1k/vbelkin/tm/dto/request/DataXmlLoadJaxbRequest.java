package ru.t1k.vbelkin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlLoadJaxbRequest extends AbstractUserRequest {

    public DataXmlLoadJaxbRequest(@Nullable String token) {
        super(token);
    }

}
