package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskRemoveByIdRequest(@Nullable String token) {
        super(token);
    }

}
