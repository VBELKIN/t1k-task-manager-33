package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String token;

    public AbstractUserRequest(@Nullable String token) {
        this.token = token;
    }

}
