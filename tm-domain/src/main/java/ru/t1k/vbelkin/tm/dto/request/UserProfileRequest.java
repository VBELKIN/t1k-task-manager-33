package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
public class UserProfileRequest extends AbstractUserRequest {

    public UserProfileRequest(@Nullable String token) {
        super(token);
    }

}
