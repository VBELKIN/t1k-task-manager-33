package ru.t1k.vbelkin.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable
    String getName();

}
