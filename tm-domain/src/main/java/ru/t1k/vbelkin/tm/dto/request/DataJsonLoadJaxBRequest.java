package ru.t1k.vbelkin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataJsonLoadJaxBRequest extends AbstractUserRequest {

    public DataJsonLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}
